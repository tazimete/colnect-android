package com.colnect.message.Api;

public class ApiEndpoints {
    public static final String BASE_URL = ApiManager.SERVER_URL_REMOTE+ApiManager.API_VERSION;

    //Registration
    public static final String GET_MESSAGE_LIST_BY_PAGINATION = BASE_URL+"messages";

}
