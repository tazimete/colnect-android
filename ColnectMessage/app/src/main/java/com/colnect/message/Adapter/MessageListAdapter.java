package com.colnect.message.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.colnect.message.Api.ApiManager;
import com.colnect.message.Model.Message;
import com.colnect.message.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.MessageListItemViewHolder> {
    private static final int VIEW_TYPE_MESSAGE = 0;
    private List<Message> messageList;

    //view holder
    public class MessageListItemViewHolder extends RecyclerView.ViewHolder{
        public LinearLayout llContainer;
        public TextView tvMessage;
        public CircleImageView ivProfilePicture;

        public MessageListItemViewHolder(@NonNull View itemView) {
            super(itemView);

            tvMessage = itemView.findViewById(R.id.tvMessage);
            ivProfilePicture = itemView.findViewById(R.id.ivProfilePicture);
            llContainer = itemView.findViewById(R.id.llContainer);
        }
    }


    public MessageListAdapter(List<Message> messageList) {
        this.messageList = messageList;
    }


    @NonNull
    @Override
    public MessageListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_MESSAGE:
                return new MessageListItemViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.message_list_item, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MessageListItemViewHolder holder, int position) {
        Message message = getItem(position);
        holder.tvMessage.setText(message.getContent());
        Picasso.get().load(ApiManager.SERVER_URL_REMOTE+message.getAuthor().getPhotoUrl()).into(holder.ivProfilePicture);

    }


    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_MESSAGE;
    }

    @Override
    public int getItemCount() {
        return messageList == null ? 0 : messageList.size();
    }

    public void addItems(List<Message> messages) {
        messageList.addAll(messages);
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        messageList.remove(position);
        notifyDataSetChanged();
    }

    public void clear() {
        messageList.clear();
        notifyDataSetChanged();
    }

    Message getItem(int position) {
        return messageList.get(position);
    }

}
