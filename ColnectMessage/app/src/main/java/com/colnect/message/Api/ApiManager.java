package com.colnect.message.Api;

import com.colnect.message.ApiServices.MessageService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiManager {
    //server url
    public static final String SERVER_URL_REMOTE = "https://message-list.appspot.com/";

    //Api version
    public static final String API_VERSION = "";

    //get retrofit client
    public static Retrofit getRetrofitClient(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiEndpoints.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }

    //get message api service
    public static MessageService getMessageApiService(){
        Retrofit retrofit = getRetrofitClient();

        MessageService messageService = retrofit.create(MessageService.class);

        return messageService;
    }
}
