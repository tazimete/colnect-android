package com.colnect.message.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MessageResponse {
    @SerializedName("count")
    private int count;

    @SerializedName("messages")
    private ArrayList<Message> messages;

    @SerializedName("pageToken")
    private String pageToken;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

    public String getPageToken() {
        return pageToken;
    }

    public void setPageToken(String pageToken) {
        this.pageToken = pageToken;
    }
}
