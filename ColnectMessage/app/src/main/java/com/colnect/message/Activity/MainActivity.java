package com.colnect.message.Activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.colnect.message.Adapter.MessageListAdapter;
import com.colnect.message.Adapter.PaginationListener;
import com.colnect.message.Adapter.RecyclerItemTouchHelper;
import com.colnect.message.Api.ApiManager;
import com.colnect.message.Model.Message;
import com.colnect.message.Model.MessageResponse;
import com.colnect.message.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    //Properties
    public static  final String CLASS_NAME = MainActivity.class.getSimpleName();
    private Toolbar toolbar;
    private String pageToken = "";
    private ArrayList<Message> messageList = new ArrayList<Message>();
    private MessageListAdapter messageListAdapter;
    private boolean isLastPage = false, isLoading = false;

    //UI
    private RecyclerView rcvMessageList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //find sub views
        findSubViews();

        //config message list view
        setupMessageLisView();

        //get message lift from server
        getMessageList();

    }

    //find view by id
    private void findSubViews(){
        toolbar = findViewById(R.id.toolbar);
        rcvMessageList = findViewById(R.id.rcvMessageList);

        setActionBar(toolbar);
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    //config message list view
    private void setupMessageLisView(){
//        rcvMessageList.setHasFixedSize(true);
        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rcvMessageList.setLayoutManager(layoutManager);
        messageListAdapter = new MessageListAdapter(messageList);
        rcvMessageList.setAdapter(messageListAdapter);

        //add scroll listerener to recycler view
        rcvMessageList.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                getMessageList();
            }
            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT,
                new RecyclerItemTouchHelper.RecyclerItemTouchHelperListener(){
                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
                        int positionToremove = viewHolder.getAdapterPosition();
                        messageListAdapter.removeItem(positionToremove);
                    }
                });
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rcvMessageList);

    }


    //get message list
    private void getMessageList(){
        Map<String, Object> params = new HashMap();
        params.put("limit",20);
        params.put("pageToken", pageToken);

        Call messageserviceCall = ApiManager.getMessageApiService().getMessageListByPagination(params);

        messageserviceCall.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {

                MessageResponse messageResponse = response.body();
                Log.d(CLASS_NAME+" -- getMessageList() --", "message list = "+messageResponse.getMessages().size());

                pageToken = messageResponse.getPageToken();

                //add message to listview
                addMessageToListView(messageResponse);
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                Log.e(CLASS_NAME, t.getMessage());
            }
        });
    }

    //add message to listview
    private void addMessageToListView(MessageResponse messageResponse){
        if (messageResponse.getMessages().size() > 0) {
            messageList.addAll(messageResponse.getMessages());
            messageListAdapter.addItems(messageList);
            messageListAdapter.notifyDataSetChanged();
            isLastPage = false;
        }else{
            isLastPage = true;
        }
    }
}
