package com.colnect.message.ApiServices;

import com.colnect.message.Api.ApiEndpoints;
import com.colnect.message.Model.MessageResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface MessageService {
    @GET("messages")
    Call<MessageResponse> getMessageListByPagination(@QueryMap Map<String, Object> options);
}
